const gulp = require('gulp')
const ts = require('gulp-typescript')
const tsProject = ts.createProject('tsconfig.json')

gulp.task('scripts', function () {
  let tsResult = gulp.src('./vtv.ts')
    .pipe(tsProject())

  return tsResult.js.pipe(gulp.dest('.'))
})
